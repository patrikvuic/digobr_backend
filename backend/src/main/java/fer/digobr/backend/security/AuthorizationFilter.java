package fer.digobr.backend.security;

import fer.digobr.backend.auth.AuthService;
import fer.digobr.backend.auth.User;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;


import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class AuthorizationFilter extends BasicAuthenticationFilter {

    public static final String JWT_HEADER_KEY = "Authorization";
    public static final String JWT_HEADER_VALUE_PREFIX = "Bearer ";

    private static final Logger LOGGER = LoggerFactory.getLogger(org.springframework.security.web.access.intercept.AuthorizationFilter.class);

    private final AuthService authService;

    public AuthorizationFilter(
            AuthenticationManager authenticationManager,
            AuthService authService) {
        super(authenticationManager);
        this.authService = authService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        // extract header
        final String headerValue = request.getHeader(JWT_HEADER_KEY);

        // validate header format
        if (headerValue == null || !headerValue.startsWith(JWT_HEADER_VALUE_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        // extract from header
        String jwt = headerValue.substring(7);

        if (JwtTokenUtil.isTokenExpired(jwt)) {
            throw new ExpiredJwtException(null, null, "Expired JWT");
        }

        try {
            // extract user id
            String username = JwtTokenUtil.getUsernameFromToken(jwt);
            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                // user is not yet logged-in

                // fetch user data
                User user = authService.getUserByUsername(username);

                // create object compatible with Principal
                UserDetails principal = new AuthenticatedUser(
                        user.getUsername(), // "username"
                        user.getPassword(), // "password"
                        Collections.singleton(new SimpleGrantedAuthority("ROLE_USER"))
                );

                // setup security context for the session associated with the user

                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(
                                principal,
                                null,
                                principal.getAuthorities());

                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource()
                        .buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        } catch (JwtException exception) {
            LOGGER.error(exception.getMessage());
            throw exception;
        }

        chain.doFilter(request, response);
    }

}
