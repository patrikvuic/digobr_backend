package fer.digobr.backend.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class AuthenticatedUser extends User {

    public AuthenticatedUser(String userId, String openId, Collection<? extends GrantedAuthority> authorities) {
        super(userId, openId, authorities);
    }


}
