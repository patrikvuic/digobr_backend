package fer.digobr.backend.word.dto;

import fer.digobr.backend.word.entity.LeaderBoardEntry;
import fer.digobr.backend.word.entity.Result;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ProfileResponse {

    private String username;
    private List<WordScoreResponse> words;
    private List<LeaderBoardEntry> leaderBoard;
}
