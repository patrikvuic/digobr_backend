package fer.digobr.backend.word.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;


@AllArgsConstructor
@Entity
@Getter
@Setter
@NoArgsConstructor
public class LeaderBoardEntry {

    @Id
    private String username;
    private Double totalScore;
    private Long rank;


}
