package fer.digobr.backend.word.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
public class WordScoreResponse {

    private String word;
    private List<Boolean> results;
    private Long elapsedMs;
    private Double score;
    private String createdAt;
}
