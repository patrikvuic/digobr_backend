package fer.digobr.backend.word.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WordResponse {

    private String word;
    private Long timeLimitMs;
}
