package fer.digobr.backend.word.dto;

import lombok.Data;

import java.util.List;

@Data
public class ResultRequest {

    private List<Boolean> results;
    private Long elapsedMs;
}
