package fer.digobr.backend.word;

import fer.digobr.backend.word.entity.LeaderBoardEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LeaderBoardRepository extends JpaRepository<LeaderBoardEntry, String> {

    @Query(value = """
        SELECT  users.username as username,
        SUM(results.score) as total_score,
        rank() OVER (ORDER BY SUM(results.score) desc) as rank
        FROM users LEFT JOIN results ON results.user_id = users.id
        GROUP BY users.username
        ORDER BY total_score desc
    """, nativeQuery = true)
    List<LeaderBoardEntry> getLeaderBoard();
}
