package fer.digobr.backend.word;

import fer.digobr.backend.security.JwtTokenUtil;
import fer.digobr.backend.word.dto.ProfileResponse;
import fer.digobr.backend.word.dto.ResultRequest;
import fer.digobr.backend.word.dto.ResultResponse;
import fer.digobr.backend.word.dto.WordResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class WordController {

    private final WordService wordService;

    @GetMapping("/word")
    public ResponseEntity<WordResponse> getRandomWord() {
        return ResponseEntity.ok(wordService.getRandomWord());
    }


    @PostMapping("/word/{word}/result")
    public ResponseEntity<ResultResponse> postResult(@PathVariable String word,
                                                     @RequestHeader(name="Authorization") String token,
                                                     @RequestBody ResultRequest request) {
        String username = JwtTokenUtil.getUsernameFromToken(token.substring(7));
        return ResponseEntity.ok(wordService.getResult(username, word, request));
    }

    @GetMapping("profile")
    public ResponseEntity<ProfileResponse> getProfile(@RequestHeader(name="Authorization") String token) {
        String username = JwtTokenUtil.getUsernameFromToken(token.substring(7));
        return ResponseEntity.ok(wordService.getProfile(username));
    }


}
