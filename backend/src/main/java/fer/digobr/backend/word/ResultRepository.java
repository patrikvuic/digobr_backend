package fer.digobr.backend.word;

import fer.digobr.backend.word.entity.LeaderBoardEntry;
import fer.digobr.backend.word.entity.Result;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultRepository extends JpaRepository<Result,Long> {


    List<Result> getResultsByUserIdAndCorrect(Long userId, boolean correct);

    List<Result> getResultsByUserId(Long userId);
}
