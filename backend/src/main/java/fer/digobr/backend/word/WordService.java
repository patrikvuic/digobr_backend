package fer.digobr.backend.word;

import com.fasterxml.jackson.databind.ObjectMapper;
import fer.digobr.backend.auth.AuthService;
import fer.digobr.backend.auth.User;
import fer.digobr.backend.exceptions.NotFoundException;
import fer.digobr.backend.word.dto.ProfileResponse;
import fer.digobr.backend.word.dto.ResultRequest;
import fer.digobr.backend.word.dto.ResultResponse;
import fer.digobr.backend.word.dto.WordResponse;
import fer.digobr.backend.word.dto.WordScoreResponse;
import fer.digobr.backend.word.entity.LeaderBoardEntry;
import fer.digobr.backend.word.entity.Result;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class WordService {

    private final AuthService authService;
    private final ResultRepository resultRepository;
    private final LeaderBoardRepository leaderBoardRepository;
    private final ObjectMapper objectMapper;

    private static final int CORRECT_POINT = 1;
    private static final double MISSED_POINT = 0.5;
    private static final double EXPIRED_POINT = 0.2;
    private static final double EXTRA_TIME_POINT = 0.3; // per second

    @Value("${digobr.app.ms-per-letter}")
    private Long msPerLetter;

    public ResultResponse getResult(String username, String word, ResultRequest request) {
        User user = authService.getUserByUsername(username);
        double score = calculateScore(word, request);
        boolean success = request.getResults().stream().filter(result -> result.equals(true)).count() == word.length();
        Result result = Result.builder()
                .word(word)
                .userId(user.getId())
                .results(request.getResults().stream().map(value -> value.toString()).collect(Collectors.joining(",")))
                .elapsedMs(request.getElapsedMs())
                .correct(success)
                .createdAt(LocalDateTime.now())
                .score(score)
                .build();
        resultRepository.save(result);
        return new ResultResponse(score);
    }

    public WordResponse getRandomWord() {
        String word = WordUtils.getRandomWord();
        Long timeLimit = word.length() * msPerLetter;
        return new WordResponse(word, timeLimit);
    }

    public ProfileResponse getProfile(String username) {
        List<LeaderBoardEntry> leaderboard = getLeaderBoard(username);
        User user = authService.getUserByUsername(username);
        List<Result> words = resultRepository.getResultsByUserId(user.getId());
        return ProfileResponse.builder()
                .username(username)
                .words(words.stream().map(word -> new WordScoreResponse(word.getWord(),
                                Arrays.stream(word.getResults().split(",")).map(Boolean::new).toList(),
                        word.getElapsedMs(),
                        word.getScore(),
                        word.getCreatedAt().toString())).toList())
                .leaderBoard(leaderboard)
                .build();
    }

    private List<LeaderBoardEntry> getLeaderBoard(String username) {
        List<LeaderBoardEntry> leaderboard = leaderBoardRepository.getLeaderBoard();
        Long userRank = leaderboard.stream().filter(entry -> entry.getUsername().equals(username)).findFirst()
                .orElseThrow(() -> new NotFoundException("User doesnt have any games")).getRank();
        return leaderboard.stream().filter(entry -> {
            if (entry.getUsername().equals(username)
                    || (entry.getRank() >= (userRank - 3) && entry.getRank() <= (userRank + 3))) {
                return true;
            }
            return false;
        }).toList();
    }

    private double calculateScore(String word, ResultRequest request) {
        double score = 0;
        for(boolean letter : request.getResults()) {
            if(letter) {
                score+=CORRECT_POINT;
            } else {
                score -= MISSED_POINT;
            }
        }
        int expiredLetters = word.length() - request.getResults().size();
        score -= expiredLetters * EXPIRED_POINT;
        long extraTime = (word.length() * msPerLetter) - request.getElapsedMs();
        if(extraTime > 0 ) {
            score += Math.ceil((double) extraTime / 1000) * EXTRA_TIME_POINT;
        }
        return score;
    }
}
