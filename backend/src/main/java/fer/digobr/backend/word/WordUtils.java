package fer.digobr.backend.word;

import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@UtilityClass
public class WordUtils {

    private List<String> words = List.of(
            "AUTO",
            "MAMA",
            "TATA",
            "OBITELJ",
            "BAKA",
            "BICIKL",
            "BUNDEVA",
            "ENCIKLOPEDIJA",
            "BOMBON",
            "ZASTAVA",
            "LAPTOP",
            "PAPAGAJ",
            "VATRA",
            "DIGITALNOOBRAZOVANJE"
    );

    public String getRandomWord() {
        Random random = new Random();
        return words.get(random.nextInt(words.size()));
    }
}
