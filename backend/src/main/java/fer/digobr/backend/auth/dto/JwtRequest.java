package fer.digobr.backend.auth.dto;

import lombok.Data;

@Data
public class JwtRequest {

    private String username;
    private String password;
}
