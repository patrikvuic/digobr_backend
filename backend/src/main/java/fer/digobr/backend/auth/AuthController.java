package fer.digobr.backend.auth;

import fer.digobr.backend.auth.dto.JwtRequest;
import fer.digobr.backend.auth.dto.RegistrationRequest;
import fer.digobr.backend.security.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("register")
    public ResponseEntity<Boolean> register(@RequestBody RegistrationRequest request) {
        return new ResponseEntity<>(authService.registerUser(request), HttpStatus.CREATED);
    }

    @PostMapping("login")
    public ResponseEntity<String> login(@RequestBody JwtRequest jwtRequest) {
        User user = authService.getUserByNameAndPassword(jwtRequest.getUsername(), jwtRequest.getPassword());
        String token = JwtTokenUtil.generateToken(user);
        return ResponseEntity.ok(token);
    }
}
