package fer.digobr.backend.auth;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsByEmailOrUsername(String email, String userName);
    Optional<User> findByUsername(String username);
}
