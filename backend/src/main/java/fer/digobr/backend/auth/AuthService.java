package fer.digobr.backend.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import fer.digobr.backend.auth.dto.RegistrationRequest;
import fer.digobr.backend.exceptions.BadRequestException;
import fer.digobr.backend.exceptions.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class AuthService {

    private final ObjectMapper objectMapper;
    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;


    public boolean registerUser(RegistrationRequest request) {
        if(userRepository.existsByEmailOrUsername(request.getEmail(), request.getUsername())){
            throw new BadRequestException("Email or username is already taken!");
        }
        User user = objectMapper.convertValue(request, User.class);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return user.getId() != null;
    }

    public User getUserByNameAndPassword(String username, String password) {
        User user =  userRepository.findByUsername(username)
                .orElseThrow(() -> new NotFoundException("User not found."));
        if(passwordEncoder.matches(password, user.getPassword())) {
            return user;
        } else {
            throw new NotFoundException("Wrong password.");
        }
    }

    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new NotFoundException("User not found."));
    }

}
