package fer.digobr.backend;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {

    @GetMapping("unrestricted")
    public ResponseEntity<String> getMessage() {
        return new ResponseEntity<>("Hai this is a normal message..", HttpStatus.OK);
    }

    @GetMapping("restricted")
    public ResponseEntity<String> getRestrictedMessage() {
        return new ResponseEntity<>("Hai this is a restricted message..", HttpStatus.OK);
    }
}
