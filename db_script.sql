--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5 (Homebrew)
-- Dumped by pg_dump version 14.5 (Homebrew)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: leader_board_entry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.leader_board_entry (
    username character varying(255) NOT NULL,
    rank bigint,
    total_score double precision
);


ALTER TABLE public.leader_board_entry OWNER TO postgres;

--
-- Name: results; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.results (
    id integer NOT NULL,
    correct boolean,
    created_at timestamp without time zone,
    elapsed_ms bigint,
    results character varying(255),
    score double precision,
    user_id bigint,
    word character varying(255)
);


ALTER TABLE public.results OWNER TO postgres;

--
-- Name: results_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.results_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.results_id_seq OWNER TO postgres;

--
-- Name: results_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.results_id_seq OWNED BY public.results.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    password character varying(100),
    email character varying(100),
    first_name character varying(255),
    last_name character varying(255),
    username character varying(255)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: results id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.results ALTER COLUMN id SET DEFAULT nextval('public.results_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: leader_board_entry; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.leader_board_entry (username, rank, total_score) FROM stdin;
\.


--
-- Data for Name: results; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.results (id, correct, created_at, elapsed_ms, results, score, user_id, word) FROM stdin;
1	f	2023-01-09 23:11:06.193479	10038	true,true,false,true	3.1	1	auto
2	t	2023-01-10 19:01:44.765738	11000	true,true,true,true	4.3	3	auto
3	f	2023-01-10 19:02:06.483949	11000	true,true,true,false	2.8	3	mama
4	t	2023-01-10 19:02:16.443365	11000	true,true,true,true	4.3	3	tata
5	t	2023-01-10 19:02:59.443854	11000	true,true,true,true	4.3	2	tata
6	t	2023-01-10 19:03:08.717234	11000	true,true,true,true	4.3	2	mama
7	t	2023-01-10 19:03:18.743064	9000	true,true,true,true	4.9	2	auto
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, password, email, first_name, last_name, username) FROM stdin;
2	$2a$10$gDgJBwkbQtdKE0C3eUMzcOX6Jpo7F4QfBWWy0.8XuSKj5sAFB2y46	matej.butkovic@gmail.com	Matej	Butkovic	matejb
3	$2a$10$LI9a9zbX8RSwRMZJmC8jSeHsSgWtnsCV.MQYvghdwXHMK9qBwEhMK	jurica.kovacevic@gmail.com	Jurica	Kovacevic	juricak
1	$2a$10$y3EJCmaYDOvHVFMoVWQoDOU3PUgDWzXTMvppQ4gS7/zPceU.8vTsm	patrik.vuic@gmail.com	Patrik	Vuic	patrikvuic
\.


--
-- Name: results_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.results_id_seq', 7, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 3, true);


--
-- Name: leader_board_entry leader_board_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.leader_board_entry
    ADD CONSTRAINT leader_board_entry_pkey PRIMARY KEY (username);


--
-- Name: results results_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.results
    ADD CONSTRAINT results_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

